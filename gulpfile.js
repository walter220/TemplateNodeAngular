/**
 * Created by Walter Jansen on 25-6-2016.
 */

'use strict';

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('gulp-browserify');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

gulp.task('sass', function () {
    return gulp.src('./src/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function() {
    return gulp.src(['./app/**/*.js', './src/js/**/*.js'])
        .pipe(concat('main.js'))
        .pipe(browserify({
            insertGlobals : true,
            debug : true
        }))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('default', ['sass', 'scripts']);

gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch(['./app/**/*.js', './src/js/**/*.js'], ['scripts']);
});

gulp.task('release', function() {
    gulp.src('./public/js/main.js')
        .pipe(uglify())
        .pipe(minify({
            ext:{
                min:'.min.js'
            },
            exclude: ['tasks']
        }))
        .pipe(gulp.dest('./public/js'));

    gulp.src('./public/css/main.css')
        .pipe(cleanCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/css'));
});

//TODO: Add image optimization from https://css-tricks.com/gulp-for-beginners/